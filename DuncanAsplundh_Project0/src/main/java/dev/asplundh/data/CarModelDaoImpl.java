//package dev.asplundh.data;
//
//import dev.asplundh.models.BigCarModel;
//import dev.asplundh.models.CarModel;
//import dev.asplundh.util.ConnectionUtil;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import dev.asplundh.models.CarSpeed;
//
//import java.sql.*;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class CarModelDaoImpl implements CarModelDao {
//
//
//
//    private Logger logger = LoggerFactory.getLogger(CarModelDaoImpl.class);
//
//    // This method grabs all the data from the "model" table from the DB
//    // It returns a list of Carmodel Objects
//    public List<CarModel> getAllModels() {
//        List<CarModel> cars = new ArrayList<>();                                        // List of CarModels
//        try (Connection connection = ConnectionUtil.getConnection()) {                  // try the connection to DB
//            Statement statement = connection.createStatement();                         // Make SQL Statement
//            ResultSet resultSet = statement.executeQuery("select ma.region, ma.name as makename, mo.name as modelname, s.engine, s.top_speed, s.zero_sixty_speed, s.mpg, s.price from make ma join model mo on mo.make_id = ma.make_id join spec s on mo.model_id = s.model_id");
//                // Create resultset and execute Query
//
//            // This loop grabs the data from the result set
//            // and stores it CarModel object, then adds those
//            // to the CarModel ArrayList to be returned.
//            while (resultSet.next()) {
//                int id = resultSet.getString("model_id");
//                String make = resultSet.getString("makename");
//                String model = resultSet.getString("modelname");
//
//
//                CarModel car = new BigCarModel(region, make, model, engine, topSpeed, zeroToSixty, mpg, price);
//                cars.add(car);
//            }
//
//        } catch (SQLException e) {                                                      // Catch if connection failed
//            logger.error(e.getClass() + " " + e.getMessage());                          // log the error
//        }
//        return cars;                                                                    // return the car list
//    }
//
//            // This method adds a given CarModel to the "model"
//            // table in the DB.
//    public CarModel addNewCarModel(CarModel model) {
//        try (Connection connection = ConnectionUtil.getConnection()) {                  // Try the connection
//            PreparedStatement preparedStatement =                                       // create a prepared statement
//                    connection.prepareStatement("insert into model (name) values (?)");
//            preparedStatement.setString(1, model.getName());
//            System.out.println(model.getName());// pass in given parameters to query
//            preparedStatement.executeQuery();                                          // execute query
//            logger.info("Adding model: " + model.getName() + " to the database..");    // log if successful
//
//
//        } catch (SQLException e) {
//            logger.error(e.getClass() + " " + e.getMessage());
//        }
//        return null;                                                                    // nothing needs to be returned
//    }
//
//        // This method updates the model table's "name" field for a
//        // given id.
//    public boolean putCarModel(String id, String name) {
//        try (Connection connection = ConnectionUtil.getConnection()) {                  // Try the connection
//            PreparedStatement preparedStatement =                                       // create a prepared sql statement
//                    connection.prepareStatement(
//                            "update model set name = (?) where model_id = (?)");
//            preparedStatement.setString(1, name);                           // pass in the "name" field
//            preparedStatement.setInt(2, Integer.parseInt(id));              // pass in "id" field
//            preparedStatement.executeQuery();                                           // execute query
//            logger.info("Car with id " + id + " updated name to " + "\"" + name + "\""); // log if successful
//        } catch (SQLException e) {
//            logger.error(e.getClass() + " " + e.getMessage());
//        }
//        return true;                                                                    // return true if successful
//    }
//
//            // This method deletes a car from the "model"
//            // table for a given name
//    public boolean deleteCarModel(String name) {
//        try (Connection connection = ConnectionUtil.getConnection()) {                  // create the connection to DB
//            PreparedStatement preparedStatement =                                       // prepare SQL statement
//                    connection.prepareStatement(
//                            "delete from model where name = (?)");
//            preparedStatement.setString(1, name);                           // pass in our name parameter
//            preparedStatement.executeQuery();                                           // execute the query
//            logger.info("Car with name " + name + " has been deleted.");                // log if successful
//        } catch (SQLException e) {
//            logger.error(e.getClass() + " " + e.getMessage());
//        }
//        return true;                                                                    // return true if successful
//    }
//
//            // This method retrieves the car's name from the "model"
//            // table, and retrieves the car's top_speed from the joined
//            // "spec" table so that they can be compared in the service
//            // method
//    public List<CarSpeed> raceByTopSpeed(String name1, String name2) {
//        List<CarSpeed> cars = new ArrayList<>();                                        // create a CarSpeed (DTO) object
//        String query = "select top_speed, name from spec s " +                          // write the query
//                "join model m on s.model_id = m.model_id " +
//                "where name = (?) or name = (?)";
//
//        try (Connection connection = ConnectionUtil.getConnection()) {                  // try connection to DB
//            PreparedStatement preparedStatement =
//                    connection.prepareStatement(query);                                 // create prepared statement
//                preparedStatement.setString(1, name1);                     // pass in first car
//                preparedStatement.setString(2, name2);                     // pass in second car
//                ResultSet resultSet = preparedStatement.executeQuery();                 // execute query
//
//                // This while block goes through the result set
//                // and stores the data in a CarSpeed object to be
//                // passed to the service method to be comapred
//                while (resultSet.next()) {
//                    String carName = resultSet.getString("name");
//                    int carTopSpeed = resultSet.getInt("top_speed");
//                    CarSpeed car = new CarSpeed(carName, carTopSpeed);
//                    cars.add(car);
//                }
//
//        } catch (SQLException e) {
//            logger.error(e.getClass() + " " + e.getMessage());
//        }
//        logger.info("Racing " + cars.get(0).getName() + " against " + cars.get(1).getName()); // log if successful
//        return cars;
//
//    }
//            // This method does the exact same as above, however it grabs
//            // the cars "zero_sixty_time" (acceleration speed) instead.
//    public List<CarSpeed> raceByAcceleration(String name1, String name2) {
//        List<CarSpeed> cars = new ArrayList<>();
//        try (Connection connection = ConnectionUtil.getConnection()) {
//            PreparedStatement preparedStatement =
//                    connection.prepareStatement(
//                            "select zero_sixty_speed, name from spec s " +
//                                    "join model m on s.model_id = m.model_id " +
//                                    "where name = (?) or name = (?)");
//            preparedStatement.setString(1, name1);
//            preparedStatement.setString(2, name2);
//            ResultSet resultSet = preparedStatement.executeQuery();
//            while (resultSet.next()) {
//                String carName = resultSet.getString("name");
//                double carAcceleration = resultSet.getDouble("zero_sixty_speed");
//                CarSpeed car = new CarSpeed(carName, carAcceleration);
//                cars.add(car);
//            }
//        } catch (SQLException e) {
//            logger.error(e.getClass() + " " + e.getMessage());
//        }
//        logger.info("Racing " + cars.get(0).getName() + " against " + cars.get(1).getName());
//        return cars;
//
//    }
//}
